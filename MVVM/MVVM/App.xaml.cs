﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MVVM
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var Content = new ContentPage
            {
                Title = "DivisasMVVM",
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children =
                {
                    new Label
                    {
                        HorizontalTextAlignment= TextAlignment.Center,
                        Text="Welcome to XamarinForms!"
                    }
                }
                }
            };

            MainPage = new NavigationPage(Content);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
